#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 20 09:48:13 2021

@author: OKE CODJO Houenangnon 
    SOUMAHRO Mohamed Kevin
"""

    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 17:19:53 2021

@author: oke
"""

import numpy as np
import matplotlib.pyplot as plt 

"""-------------------------------------------------------------------------
 SPLINE NATUREL CUBIC NON UNIFORME
----------------------------------------------------------------------------"""

# Cubic Hermite basis over [0,1] :
def H0(t) :
    y = 1 - 3 * t**2 + 2 * t**3
    return y
def H1(t) :
    y = t - 2 * t**2 + t**3
    return y    
def H2(t) :
    y = - t**2 + t**3
    return y
def H3(t) :
    y = 3 * t**2 - 2 * t**3
    return y   

# Cubic Hermite interpolation over 2 points
def HermiteC1b(x0,y0,y0p,x1,y1,y1p):
    """ Cubic Hermite interpolation of order 1 over 2 points x0 < x1
        (interpolation of value + first derivative)
        Remark : no plotting in this version HermiteC1b()
        Input :
            x0,y0,y0p,x1,y1,y1p = Hermite data of order 1 (real values)
        Output :
            x = sampling of 100 values in interval [x0,x1]
            y = image of x by the cubic Hermite interpolant
    """
    x = np.linspace(x0,x1,100)
    h = x1 - x0
    t = (x - x0) / h
    y = y0 * H0(t) + y0p * h * H1(t) + y1p * h * H2(t) + y1 * H3(t)
    return x, y

# Cubic interpolating Hermite spline C1-C1 of n points
def splineC1C1(xi,yi,yip):
    """ Hermite C1 spline interpolating 'C1 data' (xi,yi,yip)
        --> the data are C1 (Hermite data of order one)
        --> the constructed spline is C1
        Input :
            xi,yi,yip = 3 arrays of size N (points, values, derivatives)
        Output :
            xs = sampling of 100*(N-1) values in [x_first,x_last]
            ys = image of xs by the the Hermite C1 spline
    """
    N = np.size(xi)
    xs = []
    ys = []
    for i in range(N-1):
        x,y = HermiteC1b(xi[i],yi[i],yip[i],xi[i+1],yi[i+1],yip[i+1])
        xs.extend(x)
        ys.extend(y)
    return xs, ys


def splineC2NatNU(xi,yi):
    """ Natural C2 cubic spline interpolating data (xi,yi)
        NU=nonUniform -> arbitrary values xi in increasing order
        Input:
            xi,yi = vectors of data to be interpolated
        Output :
            xs = sampling of [xi_first, xi_last]
            ys = image of xs by the spline
    """
    n = np.size(xi)
    h = xi[1:n] - xi[0:n-1]
    # construction of the spline matrix :
     
    A = np.zeros((n,n))
    a = n*n 
    #Creer un vecteur de taille n*n pointant vers les valeurs de A (reshape)
    Arsh = A.reshape((a,))
    #Premier mambre de si-1"(si) = si"(xi)
    # diagonale principale <- 2 * (h_i-1 + h_i) (0<i<n)   
    (Arsh[0::n+1])[1:n-1] = 2 * (h[0:n-2] + h[1:n-1]) 
    # Premiere diagonal secondaire <- hi (0<i<n-1) 
    (Arsh[1::n+1])[1:n-1] = h[0:n-2]
    # Seconde diagonal secondaire <- hi (1<i<n) 
    (Arsh[n::n+1])[0:n-2] = h[1:n-1]
    #Premier membre s1"(x1) = sn"(xn) = 0
    #Premier et Dernier element du tableau  
    Arsh[0::a-1] = 2
    #Second et avant-dernier element 
    Arsh[1::a-3] = 1
    
    # construction of the second member :
    b = np.zeros(n)
    # Second membre de s1"(x1) = sn"(xn) = 0
    b[0] = (1/h[0]) * (yi[1]-yi[0])
    b[n-1] = (1/h[n-2]) * (yi[n-1]-yi[n-2])
    # Second membre de si-1"(si) = si"(xi)
    for i in range (1,n-1):
        hi = (h[i-1]/h[i])
        b[i] = hi *(yi[i+1]-yi[i]) + (1/hi)*(yi[i]-yi[i-1])
    b *= 3 
    # determination of derivatives yip at points xi :
    yip = np.linalg.solve(A,b)
    # evaluation of the spline :
    xs,ys = splineC1C1(xi,yi,yip)
    return (xs, ys)

"""---------------------------------------------------------------------------
 SPLINE EXPONENTIELLE SOUS TENSION 
----------------------------------------------------------------------------"""    
def phi (w, u): 
    """
    Parameters
    ----------
    w : tableau de reel 
        w = sigmai * hi
    u : Tableau de reel
        u = x-xi /hi

    Returns
    -------
    tableau de reel 
        phi_i (u) = (sinh(w*u) - u*sinh(w)) / (sinh(w)-w)

    """
    return (np.sinh(w*u) - u*np.sinh(w))/(np.sinh(w)-w)
    

def SplineEST(xi,yi,sigma):
    """
    Parameters
    ----------
    xi : Tableau de reel  de taille n 
        -> Abscisse des points'
    yi : Tableau de reel de taille n 
        -> Ordonnée des points
    
    sigma :
        -> Entier (Si sigma est globale ) 
    -> Tableau de reel de taille n-1 (sigma_i pour chaque intervalle [xi+1,xi]'
    Cette fonction permet de parammetrer chaque intervalle [xi+1,xi] avec 
    des sigma_i (la nombre d'element de sig_val doit etre le nombre d'element 
                     des xi moins 1 )
    Returns
    -------
    (xs,ys) <- xs ys Tableau de reel
    xs <- Echantillonnage de [xi,xi+1]
    ys <- Spline sous tension sur [xi,xi+1]
        Renvoie la spline sous tension passant par les points (xi,yi)  
    """
    n = np.size(xi)
    
    hi = xi[1:n] - xi[0:n-1]                           # hi = xi+1 - xi
                                                       #taille n-1 
    y = yi[1:n] - yi[0:n-1]                            # y  = yi+1 - yi 
    w = sigma * hi                                     # wi = sigmai *hi
    
    # 1<i<n
    
    
    alpha = (w*np.cosh(w)-np.sinh(w))/(np.sinh(w) - w) # alpha_i = phi'(1) 
    beta = ((w**2)*np.sinh(w))/(np.sinh(w) - w)        # beta_i = phi"(1)
    
    #delta_i = (1+alpha_i)  *  (y_i+1  - yi) / hi
    delta = (1+alpha) * (y/hi)
    
    # 1<i<n
    #       alpha_i               *hi          *beta_i-1       
    mi = (1-((alpha)[1:n-1])**2) * hi[1:n-1] * beta[0:n-2]
    
    #        alpha_i-1         *h_i-1       *beta_i    
    ni = (1-((alpha)[0:n-2])**2) * hi[0:n-2] * beta[1:n-1]

# Construction de la matrice
    
    A = np.zeros((n,n))
    a = n*n
    
    # Mettre A sous forme de ligne 
    Arsh = A.reshape((a,))
    # Remplir la diagonal principale 
    #                       alpha_i (1<i<n-2)       alpha_i (1<i<n)   
    #                                    mi(1<i<n)               ni(1<i<n)
    (Arsh[0::n+1])[1:n-1] = alpha[0:n-2]*mi[0:n-1] +alpha[1:n-1]*ni[0:n-1]
    
    # Remplir la premiere diagonal secondaire
    #                       ni (0<i<n)
    (Arsh[1::n+1])[1:n-1] = ni[0:n-1]
    
    # Remplir la seconde diagonal secondaire
    #                       mi (1<i<n)
    (Arsh[n::n+1])[0:n-2] = mi[0:n-1] 
    
    # Placer le premier element 
    #         alpha_1  
    Arsh[0] = alpha[0]
   
    # Placer 1 à la seconde et à l'avant derniere indice
    Arsh[1::a-3] = 1
    
    #Placer le dernier element 
    #           alpha_n-1  
    Arsh[a-1] = alpha[n-2]

# Construction du Second membre b
    
    b = np.zeros(n)
    #Placer le premier et le dernier element 
    #      delta_1
    b[0] = delta[0]
    #        delta_n-1 
    b[n-1] = delta[n-2]
    
    # Les element restant 
    #                     delta_i(0<i<n-1)            delta_i(1<i<n)
    #          mi(1<i<n)                 ni(1<i<n)            
    b[1:n-1] = mi[0:n-1]*delta[0:n-2] + ni[0:n-1]*delta[1:n-1]
    
    # Determiner les derivée yip
    
    yip = np.linalg.solve(A,b)    
    
    # Spline Exponentielle Sous tension
    xs = []
    ys = []
    for i in range(n-1):
        
        x = np.linspace(xi[i],xi[i+1],100)
        u = (x-xi[i]) /hi[i]
        
        # ci
        ci = (-1/(1-alpha[i]**2)) * (((1 + alpha[i])*(yi[i+1] - yi[i])) - \
                                     hi[i]*(alpha[i]*yip[i] + yip[i+1]))
        
        # di
        di = (1/(1 - alpha[i]**2)) * ((1 + alpha[i])*(yi[i+1] - yi[i]) -\
                                      hi[i]*(yip[i] + alpha[i] * yip[i+1]))
        
        # Spline sous tension ds [xi,xi+1]                         
        ysp = yi[i] *(1-u) + yi[i+1]* u +ci * phi(w[i],1-u) +di*phi(w[i],u)  
        
        # Construire la spline ds [x1,xn]
        xs.extend(x)
        ys.extend(ysp)

    return xs,ys


"""---------------------------------------------------------------------------
MAIN PROGRAM
-------------------------------------------------------------------------"""
'''
plt.close('all')

"""--------------------------------------------------------------------------
 TRACE DES FONCTIONS GENERATRICES
----------------------------------------------------------------------------"""
plt.figure(1)

# Configuration des axes
plt.xlim(0,1)
plt.ylim(-1,1)

nbt = 100
x = np.linspace(0,1,nbt)
u = x

#Trace des graphe
 
plt.plot(x,u,"b--",label = "u")
plt.plot(x,1-u,"c--",label = "1-u")

sig_val = [1,10,30,100]

for sigma in sig_val:
    plt.plot(u,phi(sigma,u),lw=1,label="phi(u) ,sigma="+str(sigma))
    plt.plot(u,phi(sigma,1-u),lw=1,label="phi(1-u) ,sigma="+str(sigma))

plt.legend(loc ="best") '''

"""----------------------------------------------------------------------------
 CAS EXPLICITE
----------------------------------------------------------------------------"""

 
plt.figure(2)
plt.suptitle("Cas Explicite",fontsize=10,x = 0.5, y = 0.05)
plt.ylim(0,1)
# COORDONNEE DES POINTS
xi = np.array([0,500,1000,2000])
yi = np.array([1,.5,.1,0])
# Placer (xi,yi) 
plt.plot(xi,yi,'co')

# Valeurs de sigma
sig_val = [.01]

for sigma in sig_val:
    xs,ys = SplineEST(xi,yi,sigma)
    plt.plot(xs,ys,lw=1,label="sigma="+str(sigma))
 
plt.legend(loc='best')   
'''
"""--------------------------------------------------------------------------
 CAS PRAMETRIQUE
-----------------------------------------------------------------------------"""

# COORDONNEE DES POINTS
xi = np.array([-2.5, -3.5, -7, -7.5, -1., 1.,  6. , 8.5, 7., 5.])
yi = np.array([-5., -7.5, -6., -0.5 , 6., -2., -4., -0., 5., 4.5])

N = np.size(xi)


# PARAMETRISATION CHORDAL

tc = np.zeros(N)
tx = np.arange(0,N-1)
c = (xi[tx+1]-xi[tx])**2
d = (yi[tx+1]-yi[tx])**2 
tc[1:N] = np.sqrt(c+d)
tc = np.cumsum(tc)
tc /= tc[N-1]

# FENETRE DES GRAPHE 
plt.figure(3,figsize=(10,5))
plt.suptitle("Cas Parametrique avec Parametrisation Chordal",fontsize=10,\
             x = 0.5, y = 0.05)

# GRAPHE A DROITE
# SIGMA GLOBALE

plt.subplot(121)
plt.title("Sigma = 1,20,50,100 ")

# Placer (xi,yi)
plt.plot(xi,yi,'co')
plt.plot(xi,yi,'c--',lw=1)

# TRACE DES COURBE  (SIGMA GLOBALE)
 
sig_val =[ 1, 20 ,50 ,100 ]
color = ['c','r','b','g']
for sigma in sig_val:
    ts,xs = SplineEST(tc,xi,sigma)
    ts,ys = SplineEST(tc,yi,sigma)
    plt.plot(xs,ys,lw=1,label="sigma = "+str(sigma))

plt.legend(loc = "best")

# GRAPHE A GAUCHE
# SIGMA PAR INTERVALLE ET SPLINE NATUREL CUBIC 
plt.subplot(122)
plt.title("Sigmai pour [xi+1,xi]")

# Placer (xi,yi)
plt.plot(xi,yi,'co')
plt.plot(xi,yi,'c--',lw=1 )

# SPLINE NATUREL CUBIC 

# Calule de la spline naturel cubic
ts,xs = splineC2NatNU(tc,xi)
ts,ys = splineC2NatNU(tc,yi) 

# Tracer de la spline naturel cubic
plt.plot(xs,ys,"r--",lw=1,label="Spline Naturel Cubique")  
 

# SIGMA 1

sigma1 = np.array([1,1,100,50,1,50,100,1,1])

# Spline Parametrique sous tension pour sigma1
ts,xs = SplineEST(tc,xi,sigma1)
ts,ys = SplineEST(tc,yi,sigma1)

# Trace de la Spline sous tension pour sigma1
plt.plot(xs,ys,"y",lw=1,label="tension spline 1") 


# Sigma 2
sigma2 = [50,100,100,1,1,1,100,100,50]

# Spline Parametrique sous tension pour sigma2
ts,xs = SplineEST(tc,xi,sigma2)
ts,ys = SplineEST(tc,yi,sigma2)

# Trace de la Spline sous tension pour sigma2
plt.plot(xs,ys,"b",lw=1,label="tension spline 2")   


plt.legend(loc="best")
 '''
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        