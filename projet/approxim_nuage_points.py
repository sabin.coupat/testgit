import numpy.polynomial.polynomial as nppol
import numpy as np
import matplotlib.pyplot as plt
from analysejson import usefuldata 


# modelise le sous-set
photo_set = usefuldata('no_gps/0b34ff9e.json')
photos = photo_set["photo_set"]["photos"]
photos_to_app = photos[0:2000]


time_of_day = [i['time_of_day'] for i in photos_to_app]
brightness = [i['brightness'] for i in photos_to_app]
daylight = [i['daylight'] for i in photos_to_app]
# approximation second degre methode des moindres carrés du nuage de points (time_of_day, brightness)
pol = nppol.polyfit(time_of_day, brightness, 2)
print(pol)

x = []
y = []
for k in range(0, 101, 1):
    val = k/100
    x.append(val)
    y.append(pol[0] + pol[1]*val + pol[2]*val*val)

# affichage des données
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

ax1.plot(time_of_day, brightness, 'b.')
ax1.set_xlabel('time_of_day')
ax1.set_ylabel('brightness', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')
ax1.plot(x, y)

ax2.plot(time_of_day, daylight, 'r.')
ax2.set_ylabel('daylight', color='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')


plt.show()
