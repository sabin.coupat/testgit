import numpy as np
import matplotlib.pyplot as plt
from geopy.distance import geodesic
from Spline import SplineEST

def latitude(duree_du_jour,jour):
    H=(duree_du_jour/48)*360 # degree
    tphy=-np.cos(H/np.tan(declinaison_soleil(jour)))
    return np.atan(tphy)

def declinaison_soleil(jour):
    return (180/np.pi)*(np.sin(np.pi*23.45/180)*np.sin(2*np.pi/365.25)*(jour-81))


def longitude(ms_photoset, ms_utc):
    resa = ms_utc - ms_photoset
    res = abs(resa)
    res = res*86400
    h = res//3600
    m = (res - 3600*h)//60
    s = res - 3600*h - 60*m
    d = h*15+(15*m)/60+(15*s)/3600
    if resa > 0:
        d= d*(-1)
    return d

#print(longitude(0.5474074074,0.75050925925 ))

def Score(Place1,Place2):
    distance  = geodesic(Place1,Place2).miles
    xi = np.array([0,500,1000,2000])
    yi = np.array([1,.5,.1,0])

    # Valeurs de sigma
    sigma = .001
    xs,ys = SplineEST(xi,yi,sigma)
    
    # Estimation du score 
    for i in range (1,len(xs)):
        if distance >= xs[i-1] and distance <= xs[i]:
            return ys[i]
    
    
    



