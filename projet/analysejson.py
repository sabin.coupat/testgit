import json


class photoset:
    # Initialisation du fichier .json
    def __init__(self, filename):
        """
        param filename: Nom du fichier json sans son extension (ou avec)
 
         
        ->photo_set("File name.json")
        construis l'objet photo_set 
         
        
        """
        self.filename = str(filename).split('/')[-1].split('.')[0]+".json"
        self.data = json.load(open(filename, 'r'))
# Methode gestion des photo 
    # Liste ou element de la liste de photo
    def get_photo(self, indice=None):
        """
        retourne la ieme photo du set si i different de None
        Sinon retourne la liste des photos du photo_set
        """
        return self.data['photo_set']["photos"][indice] if indice != None else self.data['photo_set']["photos"]
    
    def get_taille(self):
        return len (self.get_photo())

    # id
    def get_id(self, indice=None):
        """
        Renvoie le id de la ième photo si i != None
        Sinon retourne la liste des id du photos
        
        """
        if (indice is not None):
            return self.get_photo(indice)["id"]

        photos = self.get_photo()
        id = []
        for photo in photos:
            id.append(photo["id"])
        return id
    # image filename

    def get_image_filename(self, indice=None):
        """
        Renvoie le image_filename de la ième photo si i != None
        Sinon retourne la liste des image_filename du photos
        
        """
        if (indice is not None):
            return self.get_photo(indice)["image_filename"]

        photos = self.get_photo()
        image_filename = []
        for photo in photos:
            image_filename.append(photo["image_filename"])
        return image_filename
    # image shooting

    def get_image_shooting(self, indice=None):
        """
        Renvoie le image_shooting de la ième photo si i != None
        Sinon retourne la liste des image_shooting du photos
        
        """
        if (indice is not None):
            return self.get_photo(indice)["image_shooting"]

        photos = self.get_photo()
        image_shooting = []
        for photo in photos:
            image_shooting.append(photo["image_shooting"])
        return image_shooting
    # datetime

    def get_datetime(self, indice=None):
        """
        Renvoie le datetime de la ième photo si i != None
        Sinon retourne la liste des brightness du photos
        
        """
        if (indice is not None):
            return self.get_photo(indice)["datetime"]

        photos = self.get_photo()
        datetime = []
        for photo in photos:
            datetime.append(photo["datetime"])
        return datetime
    # time_of_day
    def get_time_of_day(self, indice=None):
        """
        Renvoie le time_of_day de la ième photo  si i != None 
        Sinon retourne la liste des time_of_day des photos
        """
        if (indice is not None):
            return self.get_photo(indice)["time_of_day"]

        photos = self.get_photo()
        time_of_day = []
        for photo in photos:
            time_of_day.append(photo["time_of_day"])
        return time_of_day
    # brightness
    def get_brightness(self, indice=None):
        """
        Renvoie le brightness de la ième photo si i != None
        Sinon retourne la liste des brightness du photos
        
        """
        if (indice is not None):
            return self.get_photo(indice)["brightness"]

        photos = self.get_photo()
        brightness = []
        for photo in photos:
            brightness.append(photo["brightness"])
        return brightness
    # daylight
    
    def get_daylight(self,indice=None):
        """
        Renvois le daylight de la ième photo si i != None
        Sinon retourne la liste des daylight des photos
        """
        if (indice is not None):
            return self.get_photo(indice)["attributes"]["daylight"]

        photos = self.get_photo()
        daylight = []
        for photo in photos:
            daylight.append(photo["attributes"]["daylight"])
        return daylight
  
    # All 
    def get_all(self,  *args,**kwargs):
        """
        param arg: 
            Liste des données utiles au projet excepter l'id
            "image_filename"
            "image_shooting"
            "datetime"
            "daylight"
            "brightness"
            "time_of_day"
        param kwarg:
            Concerne essentiellement la donnée "id"
            Si passé en argument (.."id" = liste des id .. )
                Renvoie les photos correspondantes
            Si passe en argument (.."id" = entier ..)
                Renvoie la photo correspondante
        Sinon
            Renvoie un dictionnaire contenant:
                -- Id su photo_set
                -- Liste des données entrée en parametres
        """
        # Toute les donnée 
        if 'all' in args or 'All' in args :
            return usefuldata(self.filename)
        else : 
            # initialiser le dictionnaire
            Dico_Usefuldata = {
                "photo_set": {
                    "id": self.data["photo_set"]["id"]
                }
            }
            d = {}
            if "id" not in kwargs :
                # Liste des info pour chaque photo
                Usefuldata_list = []
                i = 0;
                # Definir l'ensemble des donnée demandées
                for photo in self.data["photo_set"]["photos"]:
                    
                    d["id"] = self.get_id(i)
                    if "image_filename" in args:
                        d["image_filename"] = self.get_image_filename(i)
                    if "image_shooting" in args:
                        d["image_shooting"] = self.get_image_shooting(i)
                    if "time_of_day" in args:
                        d["time_of_day"] = self.get_time_of_day(i)
                    if "datetime" in args:
                        d["datetime"] = self.get_datetime(i)
                    if "brightness" in args:
                        d["brightness"] = self.get_brightness(i)
                    if "daylight" in args:
                        d["daylight"] = self.get_daylight(i)
                                
                    Usefuldata_list.append(dict(d))
                    i+=1
            
                # Ajouter la liste au dictionnaire
                Dico_Usefuldata["photos"] = Usefuldata_list
                # Renvoyer les donnée demandée
                return Dico_Usefuldata 
            else:   
                # Si l'id est passée en argument 
                # sous forme d'entier
                    if type(kwargs["id"]) == int:
                        for photo in self.get_photo():
                            if photo["id"] == kwargs["id"]:
                                d["id"] = photo["id"]
                                d["brightness"] = photo["brightness"]
                                d["datetime"] = photo["datetime"]
                                d["daylight"] = photo["attributes"]["daylight"]
                                d["image_filename"] = photo["image_filename"]
                                d["image_shooting"] = photo["image_shooting"]
                                d["time_of_day"] = photo["time_of_day"]
                                Dico_Usefuldata["photos"] = [dict(d)]
                        return Dico_Usefuldata
                # Sous forme de liste
                    elif type(kwargs["id"]) == list :
                        Usefuldata_list = []
                        
                        for photo in self.get_photo():
                            if photo["id"] in kwargs["id"]:
                                d["id"] = photo["id"]
                                if "image_filename" in args:
                                    d["image_filename"] = photo["image_filename"]
                                if "image_shooting" in args:
                                    d["image_shooting"] = photo["image_shooting"]
                                if "time_of_day" in args:
                                    d["time_of_day"] = photo["time_of_day"]
                                if "datetime" in args:
                                    d["datetime"] = photo["datetime"]
                                if "brightness" in args:
                                    d["brightness"] = photo["brightness"]
                                if "daylight" in args:
                                    d["daylight"] = photo["attributes"]["daylight"]
                                Usefuldata_list.append(dict(d))
                            Dico_Usefuldata["photos"] = Usefuldata_list
                        return Dico_Usefuldata
                    else:
                        print("[Error]")

# Donnée utile 

def usefuldata(filename):
    """
    param filename: Nom du fichier json sans son extension
    
    Returns:
        ->  Fichier filename_usefuldata.json
            --- Contient les informations utile du fichier filename.json 
        ->  Dictionnaire correspondant a ces information
    """
    data = photoset(filename)
    Dico_Usefuldata = {
        "photo_set": {
            "photos": [],
            "id": data.data["photo_set"]["id"]
        }
    }
    
    Usefuldata_list = []
    for photo in data.data["photo_set"]["photos"]:

        Usefuldata_list += [{"id": photo["id"],
               "image_filename":photo["image_filename"],
               "image_shooting":photo["image_shooting"],
               "datetime":photo["datetime"],
               "time_of_day":photo["time_of_day"],
               "brightness":photo["brightness"],
               "daylight":photo["attributes"]["daylight"]
               }]
        Dico_Usefuldata["photo_set"]["photos"] = Usefuldata_list

    # with open(data.filename, "w") as outfile:
    #     json.dump(Dico_Usefuldata, outfile, indent=3, sort_keys=True)
    return Dico_Usefuldata


def photo_set_partiel(photo_set, duree):
    """
    param photo_set: un set de photo
    param duree: duree en jour des photo_set_partiel

    Return:
        -> liste de photo_set
            Chaque photo_set a une longueur duree
    """
    data = usefuldata('no_gps/1db50fea.json')
    ps_sorted = sorted(data["photo_set"]["photos"],
                       key=lambda d: d["datetime"])
    liste_photo_set_partiel = []
    #taille_data=get_taille(self)
    #convertir la duree en datetime
    duree = duree/(86400.0)
    photo_set_partiel = []
    duree_courante = 0
    duree_ac = ps_sorted[0]["datetime"]
    duree_prec = 0
    for photo in ps_sorted:
        photo_set_partiel.append(photo)
        duree_courante += duree_ac-duree_prec
        duree_prec = duree_ac
        duree_ac = photo["datetime"]
        if(duree_courante > duree):
            liste_photo_set_partiel.append(photo_set_partiel)
            duree_courante = 0
    return liste_photo_set_partiel

def get_taille(filename):
    """
    param filename: Nom du fichier json sans son extension

    Returns:
        ->  Retourne le nombre de photo des photo_set
    """
    a = photoset(filename)
    return a.get_taille()

